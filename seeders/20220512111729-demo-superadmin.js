const bcrypt = require("bcrypt");
const { ROLES } = require("../lib/const");
const SALT_ROUND = 10;

'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    const hashedPassword = await bcrypt.hash("luhung21", SALT_ROUND);

    await queryInterface.bulkInsert("Users",[
      {
        name: 'Lalu Oki',
        email: 'example@gmail.com',
        password: hashedPassword,
        role: ROLES. SUPERADMIN,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ],
    {}
    );
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete("Users", null, {});
  },
};

// module.exports = {
//   up: (queryInterface, Sequelize) => {
//     return queryInterface.bulkInsert('Users', [{
//       name: 'Lalu Oki',
//       email: 'example@gmail.com',
//       password: 'test123',
//       createdAt: new Date(),
//       updatedAt: new Date()
//     }]);
//   },
//   down: (queryInterface, Sequelize) => {
//     return queryInterface.bulkDelete('Users', null, {});
//   }
// };
