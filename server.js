const express = require("express");
const bodyParser = require("body-parser");

const app = express();
const PORT = 3000;

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));

// middleware
const middleware = require("./middleware/auth");

// controller
const authController = require("./controller/authController");
const carsController = require("./controller/carsController");

// router
app.post("/auth/register", authController.register);
app.post(
    "/auth/regisAdmin",
    middleware.authenticate,
    middleware.isSuperAdmin,
    authController.register
    );
app.post("/auth/login", authController.login);

// CRUD
// Create Cars
app.post(
    "/cars/create",
    middleware.authenticate,
    middleware.roles,
    carsController.create
);

// getAll Cars
app.get(
    "/cars/show",
    middleware.authenticate,
    middleware.roles,
    carsController.getAll
);

// Update Cars
app.put(
    "/cars/update/:id",
    middleware.authenticate,
    middleware.roles,
    carsController.update
);

// Delete Cars
app.delete(
    "/cars/delete/:id",
    middleware.authenticate,
    middleware.roles,
    carsController.deleteCar
);

// Filter
app.get("/cars/filter?", carsController.filtered);

app.listen(PORT, () => {
    console.log(`Server berhasil berjalan di port http://localhost:${PORT}`);
});